'use strict';

const config = require('../config');
const strings = {};

for(let lang of config.i18n.lang) {
    strings[lang] = require(`./labels/${lang}.json`);
}

module.exports = strings;
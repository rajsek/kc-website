"use strict";
const express = require("express");
const crypto = require("crypto");
const request = require("request");
const path = require("path");
// const api = require("./routes/api");
const expressHbs = require("express-handlebars");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const logger = require("./lib/logger");
const cors = require("cors");
const i18n = require("i18n");
const fs = require("fs");
const config = require("./config");
// const models = require("./models");

require("dotenv").config({ path: __dirname + "/.env", silent: true });

// i18n setup
logger.log("debug", "Configuring i18n");
i18n.configure({
  locales: ["en_US"],
  directory: __dirname + "/i18n/labels",
  defaultLocale: "en_US"
});

logger.log("debug", "Registering custom functions for Handlebars");
const hbs = expressHbs.create({
  extname: "hbs",
  helpers: {
    i18n: function (text) {
      return i18n.__(text);
    }
  }
});

const isLocalDev =
  process.env.APP_ENV === "local" && process.env.NODE_ENV === "local";
const isProduction =
  process.env.APP_ENV === "production" && process.env.NODE_ENV === "production";

logger.log("debug", "Starting up");
logger.log("debug", "Getting the environment");
logger.log("info", "Loading dotenv with env = " + process.env.APP_ENV);
// logger.log('verbose', 'App env: ', process.env);

logger.log("debug", "Creating the app");
const app = express();
logger.log("debug", "Loading handlebars template engine");
app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");

//build server for local testing
if (process.env.APP_ENV === "local" && process.env.NODE_ENV === "production") {
  let compression = require("compression");
  app.use(compression());
  logger.log("info", "Running local build server");
}

app.use(
  session({
    secret: "cncbot123#",
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 600000 }
  })
);
// // app.use('/assets', express.static(__dirname + '/public/assets'));
// app.use("/public/images", express.static(__dirname + "/public/images"));
// app.use("/js", express.static(__dirname + "/public/\assets/js"));
// app.use("/template", express.static(__dirname + "/public/site"));
//app.use(express.static(path.join(__dirname, "site")));
//app.use('/', express.static(path.resolve('./site'), { maxAge: 86400000 }));

app.use('/', express.static(path.resolve('./site'), { maxAge: 86400000 }));

app.use(
  bodyParser.json({
    verify: verifyRequestSignature
  })
);

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

const server = require("http").Server(app);
// app.use("/api", api);

// //set all model associations
// models.routineGroup.hasMany(models.routine, {
//   foreignKey: "routineGroupId"
// });

// models.routineGroup.belongsTo(models.product, {
//   foreignKey: "productId"
// });

/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
  const signature = req.headers["x-hub-signature"];

  if (!signature) {
    // For testing, let's log an error. In production, you should throw an
    // error.
    console.error("Couldn't validate the signature.");
  } else {
    const elements = signature.split("=");
    const method = elements[0];
    const signatureHash = elements[1];

    const expectedHash = crypto
      .createHmac("sha1", config.appSecret)
      .update(buf)
      .digest("hex");
    if (signatureHash != expectedHash) {
      throw new Error("Couldn't validate the request signature.");
    }
  }
}
/*
if (isLocalDev) {
  //webpack for local development
  const webpack = require("webpack"),
    webpackDevMiddleware = require("webpack-dev-middleware"),
    webpackHotMiddleware = require("webpack-hot-middleware"),
    webpack_config = require("./webpack.config.js"),
    compiler = webpack(webpack_config),
    indexFile = path.resolve(webpack_config.output.path, "index.hbs"),
    indexTemplateFile = path.resolve(
      webpack_config.output.path,
      "index-template.hbs"
    );

  console.log(indexFile);

  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: webpack_config.output.publicPath,
      inline: true,
      stats: "minimal"
    })
  );

  app.use(webpackHotMiddleware(compiler));
  app.get("*", (req, res, next) => {
    compiler.outputFileSystem.readFile(indexFile, (error, result) => {
      if (error) {
        return next(error);
      }
      fs.writeFile(indexTemplateFile, result, function (err) {
        let location, date;
        let context = {};

        if (err) {
          return next(err);
        }
        app.set("views", __dirname + "/public/build");
        context.defaultLocale = "en_US";
        context.app_env = process.env.APP_ENV;
        context.cdnUrl = process.env.CDN_DOMAIN_URL;
        context.debugText = `Test`;
        i18n.setLocale(context.defaultLocale);
       // res.render("index-template", context);
      });
    });
  });
} else {
  //other envs
  let BUILD_DIR = path.resolve(__dirname, "public/build");
  app.use(express.static(BUILD_DIR));

  app.get("*", (req, res, next) => {
    app.set("views", __dirname + "/public/build");
    let context = {};
    context.defaultLocale = "en_US";
    context.app_env = process.env.APP_ENV;
    context.cdnUrl = process.env.CDN_DOMAIN_URL;
    context.debugText = `Test`;
    i18n.setLocale(context.defaultLocale);
    res.render("index", context);
  });
}

//Authorize messenger bot on first call
app.get("/webhook", function (req, res) {
  if (
    req.query["hub.mode"] === "subscribe" &&
    req.query["hub.verify_token"] === config.verifyToken
  ) {
    console.log("Validating webhook");
    res.status(200).send(req.query["hub.challenge"]);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});
*/

const host = process.env.HOST || '0.0.0.0'
const serverPort = process.env.PORT || process.env.APP_PORT || 8080;

server.listen(serverPort, host, () => {
  console.log(`listening at port ${serverPort}`);
});

"use strict";
//When user upload an image, Granb teh attachment link
//and find the mood of the persion(Happy , sad etc) using Google cloud vision API
const AWS = require("aws-sdk");
const path = require("path");
const config = require("../config");
const models = require("../models");
const Promise = require("bluebird");
const uniqid = require("uniqid");
const Sequelize = require("sequelize");
const fs = Promise.promisifyAll(require("fs"));
const aws = require("../lib/aws");
const logger = require("../lib/logger");

const _ = require("lodash");

exports.isValidString = str => {
  return Boolean(str) && !_.isNull(str) && !_.isEmpty(str.toString().trim());
};

exports.test = (locale, imageUrl, trackLink, trackName, pageId, user) => {};

exports.isValidWebsite = website => {
  const websiteRegex = /^(http[s]?:\/\/){1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
  return websiteRegex.test(website);
};
exports.escapeRegExp = str => {
  return str ? str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") : str;
};

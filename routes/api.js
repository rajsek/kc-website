'use strict'

const express = require('express');
const router = express.Router();
const i18n = require('i18n');
const Promise = require('bluebird');
const config = require('../config.js')
const logger = require('../lib/logger');
const controller = require('./controller');
const default_locale = config.i18n.default_lang;
const utils = require('./utils');
const models = require('../models');
const crypto = require('crypto');
const Sequelize = require('sequelize');
const util = require('util');
// generating a hash for dev purpose
const generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
const validPassword = function (password) {
    return bcrypt.compareSync(password, config.admin.password);
};
//console.log(generateHash('1234Admin#'));
const checkAdminAuth = function (req, res, next) {
    // if (req.session && !req.session.isLoggedIn) {
    //     return res.end(JSON.stringify({
    //         login: false
    //     }));
    // }
    // const userCookie = req.cookies.user;
    // if (!userCookie) {
    //     return res.end(JSON.stringify({
    //         login: false
    //     }));
    // }
    next();
}

// Otherwise, returns the label as is.
function getTranslation(label, locale) {
    var catalog = i18n.getCatalog(locale);

    if (catalog && catalog[label] !== undefined && catalog[label] !== null) {
        return catalog[label];
    } else {
        return label;
    }
}

//logout
router.get('/logout', function (req, res, next) {
    res.clearCookie("user");
    req.session.destroy();
    return res.redirect('/');
});

//Return the Error message in JSON format
function newErrResponse(errCode, locale) {
    return JSON.stringify({
        success: false,
        errCode: errCode, // raw error code, corresponding to one of the UploadError
        msg: getTranslation(errCode, locale) // translated error message
    });
}

router.post('/saveRoutineGroup', function (req, res, next) {

    controller.saveRoutineGroup(req)
        .then(data => {
            return utils.getCollectionData('routineGroup')
        }).then(data => {
            if (data)
                return res.status(200).json({
                    success: true,
                    routineGroup: data
                });
            else
                return res.end(newErrResponse('error_invalid_params', default_locale));
        }).catch(err => {
            logger.info(`Error Occurred in /api/saveRoutine' route: ${util.inspect(err, false, 2, true)}`);
            res.end(newErrResponse('invalid_request', default_locale));
        })
})
router.post('/checkLogin', function (req, res) {
    if (!req.body) {
        res.status(400);
        res.end(newErrResponse('invalid_request', default_locale));
    }

    if (!req.body.userName) {
        res.status(400);
        res.end(newErrResponse('error_empty_user_name', default_locale));
        return;
    }
    if (!req.body.password) {
        res.status(400);
        res.end(newErrResponse('error_empty_password', default_locale));
        return;
    }
    if (req.body.userName != config.admin.username || !validPassword(req.body.password)) {
        // res.status(400);
        res.end(newErrResponse('error_invalid_credentials', default_locale));
        return;
    }
    const user = crypto.createHash('md5').update(req.body.userName).digest('hex');
    res.cookie("user", user, { maxAge: 1 * 24 * 60 * 60 * 1000, httpOnly: false });
    req.session.isLoggedIn = true; // set session
    return res.end(JSON.stringify({
        success: true
    }));
});
module.exports = router;

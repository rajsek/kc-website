/*
* Don't use ES6 features for this module
* because it is read by Webpack's config
* which doesn't support ES6 out-of-the-box.
*/

const conf = {
  i18n: {
    langs: ["en_US"],
    default_lang: "en_US"
  },
  defaultCountry: "US",
  routePrefix: {
    api: "/api"
  }
};
switch (app_env) {
  case "development":
    conf.fbAppId = "xxx";
    break;
  case "preproduction":
    conf.fbAppId = "xxx";
    break;
  case "staging":
    conf.fbAppId = "xxx";
    break;
  case "production":
    conf.fbAppId = "xxx";
    break;
  case "local":
  default:
    conf.fbAppId = "xxx";
}

module.exports = conf;

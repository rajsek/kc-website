import React from "react";
import PropTypes from "prop-types";
import { Component } from "react";

import Header from "../components/Header";
import Footer from "../components/Footer";
import { browserHistory } from "react-router";
import * as actions from "../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import curryRight from "lodash.curryright";
import { Link } from "react-router";
import Alert from "react-s-alert";

import {
  defineMessages,
  FormattedMessage,
  FormattedDate,
  injectIntl,
  intlShape
} from "react-intl";
import RaisedButton from "material-ui/RaisedButton";

const labels = defineMessages({
  title: {
    id: "home_title",
    defaultMessage: "<small>Home Title</strong>"
  }
});
window.onpopstate = function (event) {
  browserHistory.push(`/`);
};

class App extends Component {
  componentWillMount() { }

  componentDidMount() {
    setTimeout(
      () => document.getElementById("loader_wrap").classList.add("hide"),
      500
    );
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { isLoggedIn, selectedTab } = this.props;
    return (
      <div className="page">
        {/*
      <!--
      ========================================================
                              HEADER
      ========================================================
      -->
      */}
        <Header />
        {this.props.children}
        {/* <Footer />
        {isLoggedIn ? <Footer selectedTab={selectedTab} /> : null}
        <Alert stack={{ limit: 4 }} /> */}
      </div>
    );
  }
}

//Gran store from context like connect() does it.
App.contextTypes = {
  store: PropTypes.object.isRequired,
  intl: intlShape.isRequired
};

const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
  const isLoggedIn = state.getIn(["home", "isLoggedIn"]);
  const selectedTab = state.getIn(["home", "selectedTab"]);
  return {
    isLoggedIn,
    selectedTab
  };
}

export default compose(
  injectIntlDecorator(),
  connect(mapStateToProps, actions, null, { pure: false })
)(App);

import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import curryRight from "lodash.curryright";
import { browserHistory } from "react-router";
import * as actions from "../actions";
import LoginForm from "./LoginForm";
import { AppBar, MenuItem } from "material-ui";
require('jquery');
require('../../assets/js/parallax');

import Parallax from '../../assets/js/parallax';

import Drawer from "material-ui/Drawer";
import FontIcon from "material-ui/FontIcon";
import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator,
  ToolbarTitle
} from "material-ui/Toolbar";
import FlatButton from "material-ui/FlatButton";

import {
  defineMessages,
  FormattedMessage,
  FormattedDate,
  injectIntl,
  intlShape
} from "react-intl";

const labels = defineMessages({
  test: {
    id: "test",
    defaultMessage: "Test"
  }
});


class Header extends Component {
  constructor() {
    super();
    this.state = {
      value: 3
    };
  }
  componentWillMount() { }
  componentWillUpdate(nextProps) { }

  componentDidMount() {
    //this.parallax = this.scene ? new Parallax(this.scene) : null;
    (function ($) {
      var o = $('.rd-parallax');
      if (o.length) {
        $(document).ready(function () {
          o.each(function () {
            if (!$(this).parents(".swiper-slider").length) {
              console.log(isIE(), isIE2());
              if (!isIE2()) {
                var paral = new Parallax(o);
              } else {
                for (i = 0; i < o.length; i++) {
                  var parallax = $(o[i]);
                  var imgPath = parallax.find(".rd-parallax-layer" + "[data-type=media]").first().attr("data-url");
                  parallax.css({
                    "background-image": 'url(' + imgPath + ')',
                    "background-attachment": "fixed",
                    "background-size": "cover"
                  });
                }
              }
            }
          });
        });
      }
    })(jQuery);

  }
  componentWillUnmount() {
    this.parallax.disable()
  }

  handleChange = (event, index, value) => this.setState({ value });

  submit = values => {
    const data = values.toJS();
    return this.props.checkLogin(data);
  };
  handleTouchTap = task => {
    this.props.openNav(true);
  };

  render() {
    var menuItems = [
      { route: "get-started", text: "Get Started" },
      { route: "customization", text: "Customization" },
      { route: "components", text: "Components" }
      // { type: MenuItem.Types.SUBHEADER, text: 'Resources' },
      // {
      //     type: MenuItem.Types.LINK,
      //     payload: 'https://github.com/callemall/material-ui',
      //     text: 'GitHub'
      // },
      // {
      //     text: 'Disabled',
      //     disabled: true
      // },
      // {
      //     type: MenuItem.Types.LINK,
      //     payload: 'https://www.google.com',
      //     text: 'Disabled Link',
      //     disabled: true
      // },
    ];
    const { formatMessage } = this.props.intl;
    return (<header className="page-header dark">

      {/*<!--RD Navbar-->*/}
      <div className="rd-navbar-wrap">
        <nav className="rd-navbar bg-white" data-layout="rd-navbar-fixed" data-hover-on="false" data-stick-up="false" data-sm-layout="rd-navbar-fullwidth"
          data-md-layout="rd-navbar-static">
          {/*<!--<div className="rd-navbar-top-panel">
              <div className="rd-navbar-inner">
                <button data-rd-navbar-toggle=".list-inline, .fa-envelope, .fa-phone, .material-icons-shopping_cart" className="rd-navbar-collapse-toggle"><span></span></button><a href="mailto:#" className="fa-envelope">demomail@demolink.org</a><a href="callto:#" className="fa-phone">+1 (126) 598-89-75</a>
                <ul className="list-inline pull-right">
                  <li><a href="#" className="fa-facebook"></a></li>
                  <li><a href="#" className="fa-pinterest-p"></a></li>
                  <li><a href="#" className="fa-twitter"></a></li>
                  <li><a href="#" className="fa-google-plus"></a></li>
                  <li><a href="#" className="fa-instagram"></a></li>
                </ul>
              </div>
            </div>-->*/}
          <div className="rd-navbar-inner">

            {/*<!--RD Navbar Panel-->*/}
            <div className="rd-navbar-panel">

              {/*<!--RD Navbar Toggle-->*/}
              <button data-rd-navbar-toggle=".rd-navbar" className="rd-navbar-toggle"><span></span></button>
              {/*<!--END RD Navbar Toggle-->*/}

              {/*<!--RD Navbar Brand-->*/}
              <div className="rd-navbar-brand">
                <a href="index.html" className="brand-name">
                  <img src="/assets/images/KClogo.png" alt="" width="75" />
                  <span style={{ whiteSpace: "nowrap", color: "#1c8751", fontWeight: "bold" }}>Karthikeya Construction</span>
                </a>
              </div>
              {/*<!--END RD Navbar Brand-->*/}
            </div>
            {/*<!--END RD Navbar Panel-->*/}

            <div className="rd-navbar-nav-wrap"><a href="shop-cart.html" className="material-icons-shopping_cart"></a>
              {/*<!--RD Navbar Search-->*/}
              <div className="rd-navbar-search">
                <form action="search.php" method="GET" className="rd-navbar-search-form">
                  <label className="rd-navbar-search-form-input">
                    <input type="text" name="s" placeholder="Search.." autoComplete="off" />
                  </label>
                  <button type="submit" className="rd-navbar-search-form-submit"></button>
                </form><span className="rd-navbar-live-search-results"></span>
                <button data-rd-navbar-toggle=".rd-navbar-search, .rd-navbar-live-search-results" className="rd-navbar-search-toggle"></button>
              </div>
              {/*<!--END RD Navbar Search-->*/}

              {/*<!--RD Navbar Nav-->*/}
              <ul className="rd-navbar-nav">
                <li className="active"><a href="./">Home</a></li>
                <li><a href="#">Elements</a>

                  {/*<!--RD Navbar Dropdown-->*/}
                  <ul className="rd-navbar-dropdown">
                    <li><a href="accourdions_tabs.html">Tabs & Accordions</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="forms.html">Forms</a></li>
                    <li><a href="buttons.html">Buttons</a></li>
                    <li><a href="grid.html">Grid</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="tables.html">Tables</a></li>
                    <li><a href="progress_bars.html">Progress bars</a></li>
                  </ul>
                  {/*<!--END RD Navbar Dropdown-->*/}

                </li>
                <li><a href="#">Features</a>
                  <ul className="rd-navbar-dropdown">
                    <li><a href="index-parallax.html">Header Parallax</a></li>
                    <li><a href="index-2.html">Header Transparent</a></li>
                    <li><a href="index-cent.html">Header Center, Footer Light</a></li>
                    <li><a href="index-min.html">Header Minimal, Footer Light</a></li>
                    <li><a href="index-topbar.html">Header Corporate</a></li>
                    <li><a href="index-sidebar.html">Header Hamburger Menu</a></li>
                    <li><a href="index-cent-dark.html">Footer Center Dark</a></li>
                    <li><a href="index-min-dark.html">Footer Minimal Dark</a></li>
                    <li><a href="index-wid.html">Footer Widget Light</a></li>
                    <li><a href="index-wid-dark.html">Footer Widget Dark</a></li>
                  </ul>
                </li>
                <li><a href="#">Extras</a>
                  <ul className="rd-navbar-dropdown">
                    <li><a href="404.html">404</a></li>
                    <li><a href="coming.html">Coming soon</a></li>
                    <li><a href="login.html">Login page</a></li>
                    <li><a href="maintenance.html">Maintenance page</a></li>
                    <li><a href="register.html">Register page</a></li>
                    <li><a href="search-res.html">Search results page</a></li>
                    <li><a href="terms.html">Terms of Service</a></li>
                  </ul>
                </li>
                <li className="dropdown-megamenu"><a href="#">Pages</a>

                  {/*<!--RD Navbar Megamenu-->*/}
                  <ul className="rd-navbar-megamenu">
                    <li>
                      <p>Pages</p>
                      <ul>
                        <li><a href="index-2.html">Home 2</a></li>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="our-team.html">Our Team</a></li>
                        <li><a href="team-member.html">Team member profile</a></li>
                        <li><a href="contact_us.html">Contact Us</a></li>
                        <li><a href="contact_us_2.html">Contact Us 2</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="services.html">Services page</a></li>
                        <li><a href="clients.html">Clients Page</a></li>
                        <li><a href="faq.html">FAQ Page</a></li>
                      </ul>
                    </li>
                    <li>
                      <p>Pages 2</p>
                      <ul>
                        <li><a href="left_sidebar.html">With left sidebar</a></li>
                        <li><a href="right_sidebar.html">With right sidebar</a></li>
                        <li><a href="no_sidebar.html">Without sidebar</a></li>
                        <li><a href="blog_default.html">Default Blog</a></li>
                        <li><a href="blog_columns_2.html">2 Columns Blog</a></li>
                        <li><a href="blog_columns_3.html">3 Columns Blog</a></li>
                        <li><a href="blog_archive.html">Archive page</a></li>
                        <li><a href="blog_post.html">Post page</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                      </ul>
                    </li>
                    <li>
                      <p>Layouts</p>
                      <ul>
                        <li><a href="index-2.html">Header Transparent</a></li>
                        <li><a href="index-cent.html">Header Center, Footer Light</a></li>
                        <li><a href="index-min.html">Header Minimal, Footer Light</a></li>
                        <li><a href="index-topbar.html">Header Corporate</a></li>
                        <li><a href="index-sidebar.html">Header Hamburger Menu</a></li>
                        <li><a href="index-cent-dark.html">Footer Center Dark</a></li>
                        <li><a href="index-min-dark.html">Footer Minimal Dark</a></li>
                        <li><a href="index-wid.html">Footer Widget Light</a></li>
                        <li><a href="index-wid-dark.html">Footer Widget Dark</a></li>
                      </ul>
                    </li>
                    <li>
                      <p>E-commerce</p>
                      <ul>
                        <li><a href="shop-catalog.html">Product catalog</a></li>
                        <li><a href="shop-product.html">Single product</a></li>
                        <li><a href="shop-cart.html">Shopping cart</a></li>
                        <li><a href="shop-checkout.html">Checkout</a></li>
                      </ul>
                    </li>
                  </ul>
                  {/*<!--END RD Navbar Megamenu-->*/}
                </li>
                <li><a href="#">Blog</a>
                  <ul className="rd-navbar-dropdown">
                    <li><a href="blog_default.html">Default Blog</a></li>
                    <li><a href="blog_columns_2.html">2 Columns Blog</a></li>
                    <li><a href="blog_columns_3.html">3 Columns Blog</a></li>
                    <li><a href="blog_archive.html">Archive page</a></li>
                    <li><a href="blog_post.html">Post page</a></li>
                    <li><a href="timeline.html">Timeline <span className="marked">hot</span></a></li>
                  </ul>
                </li>
                <li><a href="#">Gallery</a>
                  <ul className="rd-navbar-dropdown">
                    <li><a href="gallery-1.html">Grid Padding Gallery</a></li>
                    <li><a href="gallery-2.html">Grid No Padding Gallery</a></li>
                    <li><a href="gallery-3.html">Grid Masonry</a></li>
                    <li><a href="gallery-4.html">Grid Cobbles</a></li>
                  </ul>
                </li>
                <li><a href="#">Shop</a>
                  <ul className="rd-navbar-dropdown">
                    <li><a href="shop-catalog.html">Product catalog</a></li>
                    <li><a href="shop-product.html">Single product</a></li>
                    <li><a href="shop-cart.html">Shopping cart</a></li>
                    <li><a href="shop-checkout.html">Checkout</a></li>
                  </ul>
                </li>
              </ul>
              {/*<!--END RD Navbar Nav-->*/}
            </div>
          </div>
        </nav>
      </div>
      {/*<!--END RD Navbar-->*/}
      <section>
        {/*<!--Swiper-->*/}
        <div ref={el => this.scene = el} className="rd-parallax">
          <div data-speed="0.6" data-type="media" data-url="assets/images/header-1.jpg" className=" r"></div>
          <div data-speed="0.78" data-type="html" data-fade="true" className="well-parallax jumbotron text-center rd-parallax-layer">
            <h1>10 YEARS<small>of EXPERIENCE  IN THE INDUSTRY</small></h1>
            <p className="big">At Construction we strive to excel in every service we provide, adding value for our customers wherever<br /> possible,
              and thereby attaining national leadership in the construction industry.</p>
            <div className='btn-group-variant'>
              <a className='btn btn-default round-xl btn-sm' href='#'>SEE ALL PROJECTS</a> <a className='btn btn-default round-xl btn-sm'
                href='#'>about us</a></div>
          </div>
        </div>
      </section>
    </header>
    );
  }
  componentWillUnmount() { }
}

Header.propTypes = {
  intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
  const temp = state.getIn(["home", "temp"]);
  const isNavigationOpen = state.getIn(["home", "isNavigationOpen"]);
  return {
    temp,
    isNavigationOpen
  };
}

export default compose(
  injectIntlDecorator(),
  connect(mapStateToProps, actions, null, { pure: false })
)(Header);

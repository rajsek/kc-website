import React, { Component } from 'react';
import * as actions from '../actions';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import conf from '../conf';
import PropTypes from 'prop-types';
import FontIcon from 'material-ui/FontIcon';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';
import { browserHistory } from 'react-router';

import { BottomNavigation, BottomNavigationItem } from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
const pushIcon = <FontIcon className="material-icons">notifications_active</FontIcon>;
const tipsIcon = <FontIcon className="material-icons">thumb_up</FontIcon>;
const nearbyIcon = <IconLocationOn />;
const productIcon = <FontIcon className="material-icons">shopping_cart</FontIcon>;
const voucherIcon = <FontIcon className="material-icons">redeem</FontIcon>;

const addRoutineIcon = <FontIcon className="material-icons">note_add</FontIcon>;


import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';

const labels = defineMessages({
    temp: {
        id: 'temp',
        defaultMessage: 'temp'
    }
});

class Footer extends Component {
    state = {
        selectedIndex: 0,
    };

    select = (index) => {
        this.props.changeSelectedTab(index);

        //this.setState({ selectedIndex: index });
    }
    handleActive(tab) {
        alert(`A tab with this route property ${tab.props.route} was activated.`);
    }
    render() {
        const { formatMessage } = this.props.intl;
        const { selectedTab } = this.props;
        return (
            <footer id="footer">  <Paper zDepth={1}>
                <BottomNavigation selectedIndex={selectedTab}>
                    <BottomNavigationItem
                        label="Routine"
                        icon={addRoutineIcon}
                        onTouchTap={() => this.select(0)}
                    />
                    <BottomNavigationItem
                        label="Product"
                        icon={productIcon}
                        onTouchTap={() => this.select(1)}
                    />

                    {/* <BottomNavigationItem
                        label="Routine"
                        icon={addRoutineIcon}
                        onTouchTap={() => this.select(0)}
                    /> */}
                    <BottomNavigationItem
                        label="Notification"
                        icon={pushIcon}
                        onTouchTap={() => this.select(2)}
                    />
                    <BottomNavigationItem
                        label="Tips"
                        icon={tipsIcon}
                        onTouchTap={() => this.select(3)}
                    />
                    <BottomNavigationItem
                        label="Voucher"
                        icon={voucherIcon}
                        onTouchTap={() => this.select(4)}
                    />
                    {/* <BottomNavigationItem
                        label="Nearby"
                        icon={nearbyIcon}
                        onTouchTap={() => this.select(3)}
                    /> */}

                </BottomNavigation>
            </Paper></footer>
        );
    }
}

Footer.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

export default compose(injectIntlDecorator(),
    connect(null, actions, null, { pure: false })
)(Footer);

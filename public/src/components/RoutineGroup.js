import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import curryRight from "lodash.curryright";
import { browserHistory } from "react-router";
import * as actions from "../actions";
import { Link } from "react-router";
import RaisedButton from "material-ui/RaisedButton";
import FontIcon from "material-ui/FontIcon";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import AddRoutineGroup from "./Form/AddRoutineGroup";
import LoginForm from "./LoginForm";

import Immutable from "immutable";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

import RoutineGroupTable from "./Table/RoutineGroupTable";

import {
  defineMessages,
  FormattedMessage,
  FormattedDate,
  injectIntl,
  intlShape
} from "react-intl";

const labels = defineMessages({
  test: {
    id: "test",
    defaultMessage: "Test"
  }
});
class RoutineGroup extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.getData("routineGroup");
  }
  componentWillUpdate(nextProps) {}

  componentDidMount() {}
  submit = values => {
    const data = values.toJS();
    return this.props.saveRoutineGroup(data);
  };

  render() {
    const actionsButton = [
      <FlatButton
        label="Save"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => this.props.submitAddFrom("addRoutineGroup")}
      />,
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={() => this.props.setModelPopupVisibility()}
      />
    ];
    const { formatMessage } = this.props.intl;
    const { addRoutineGroup } = this.props;
    return (
      <div>
        <RaisedButton
          onClick={() => {
            //browserHistory.push(`home/addRoutineGroup`)
            this.props.setModelPopupVisibility("addRoutineGroup");
          }}
          label="Add"
          icon={
            <FontIcon className="material-icons">add_circle_outline</FontIcon>
          }
          primary={true}
          style={{ margin: "12px 0px" }}
        />
        <Dialog
          title="Add New Routine Group"
          actions={actionsButton}
          modal={false}
          open={addRoutineGroup}
          onRequestClose={() => this.props.setModelPopupVisibility()}
          autoScrollBodyContent={true}
        >
          <AddRoutineGroup onSubmit={this.submit} />
        </Dialog>
        <RoutineGroupTable data={this.props.routineGroup} />
      </div>
    );
  }
  componentWillUnmount() {}
}

RoutineGroup.propTypes = {
  intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
  const routineGroup = state.getIn(["home", "routineGroup"]);
  const modelPopUpId = state.getIn(["home", "modelPopUpId"]);
  const showModelPopup = state.getIn(["home", "showModelPopup"]);
  const addRoutineGroup = state.getIn([
    "home",
    "showAddModelPopup",
    "addRoutineGroup"
  ]);

  return {
    routineGroup: (routineGroup && routineGroup.toJS()) || [],
    showModelPopup: showModelPopup,
    modelPopUpId: (modelPopUpId && modelPopUpId.toJS()) || [],
    addRoutineGroup: addRoutineGroup || false
  };
}

export default compose(
  injectIntlDecorator(),
  connect(mapStateToProps, actions, null, { pure: false })
)(RoutineGroup);

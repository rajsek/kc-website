import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import { browserHistory } from 'react-router';
import * as actions from '../../actions';
import TextField from 'material-ui/TextField';
import { Field, reduxForm } from 'redux-form/immutable'
import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';
import conf from '../../conf';
import Avatar from 'material-ui/Avatar';

const labels = defineMessages({
    test: {
        id: 'test',
        defaultMessage: "Test"
    }
});
const style = {
    margin: 12,
};

const validate = values => {
    const errors = {}
    const requiredFields = ['name', 'skinType', 'skinIssue', 'product']
    requiredFields.forEach(field => {
        if (!values.get(field)) {
            errors[field] = 'Required'
        }
    })
    return errors
}

const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {

    return <TextField hintText={label} floatingLabelText={label} errorText={touched && error} {...input}{...custom} />
}
const renderFileField = ({ input, label, accept, type, meta: { touched, error } }) => {
    delete input.value;
    return <div className={touched && error ? "has-error form-group form-inline" : "form-group form-inline"} >
        <br />
        <label htmlFor={input.name}>{label} :        <input {...input} type={type} accept={accept} /></label>
        {touched && error ? <span className="text-danger text-center"><label></label> <span>{error}</span></span> : null}
    </div >
}
class AddRoutine extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.getData('product');
    }
    render() {
        const { formatMessage } = this.props.intl;
        const { handleSubmit, pristine, error, reset, submitting, params } = this.props
        const { product } = this.props;
        return (
            <div>
                <form style={{ paddingBottom: "25px", margin: "0px 25px" }} onSubmit={handleSubmit}>
                    {error && <div className="alert alert-danger text-center">{error}</div>}
                    <div><Field fullWidth={true} name="title" component={renderTextField} label="Title" /></div>
                    <div><Field fullWidth={true} name="description" component={renderTextField} label="Description" multiLine={true} rows={2} /></div>
                    <div><Field fullWidth={true} name="link" component={renderTextField} label="Link" /></div>
                    <div><Field fullWidth={true} name="fileUploads"
                        component={renderFileField} type="file" label="Upload Picture" accept='image/*'
                        multiple={false} />
                    </div>
                </form>
            </div>
        );
    }
    componentWillUnmount() {
    }
}

AddRoutine.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
    const product = state.getIn(['home', 'product']);
    const routineGroup = state.getIn(['home', 'routineGroup']);

    return {
        routineGroup: routineGroup && routineGroup.toJS() || [],
        product: product && product.toJS() || []
    }
}

export default compose(injectIntlDecorator(),
    reduxForm({ form: 'addRoutine', validate }),
    connect(mapStateToProps, actions, null, { pure: false })
)(AddRoutine);
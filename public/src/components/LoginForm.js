import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import { browserHistory } from 'react-router';
import * as actions from '../actions';
import { Field, reduxForm } from 'redux-form/immutable'
import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';

const labels = defineMessages({
    test: {
        id: 'test',
        defaultMessage: "Test"
    }
});

const validate = values => {
    const errors = {}
    if (!values.get('userName')) {
        errors.userName = 'Required'
    }
    if (!values.get('password')) {
        errors.password = 'Required'
    }
    return errors
}

const renderField = ({ input, label, type, meta: { touched, error, warning } }) =>
    <div className={touched && error ? "has-error form-group form-inline" : "form-group form-inline"}>
        <label htmlFor={input.name}>{label} : </label>
        <input className={touched && error ? "form-control textBox" : "form-control textBox"}  {...input} placeholder={label} type={type} />
        {touched && error ? <span className="text-danger text-center"><label></label> <span>{error}</span></span> : null}
    </div>
const renderSelectField = ({ input, label, type, meta: { touched, error, warning }, children }) =>
    <div className={touched && error ? "has-error form-group form-inline" : "form-group form-inline"}>
        <label htmlFor={input.name}>{label} : </label>
        <select required {...input} className={touched && error ? "form-control textBox" : "form-control textBox"}>
            {children}
        </select>
        {touched && error ? <span className="text-danger text-center"><label></label> <span>{error}</span></span> : null}
    </div>

class LoginForm extends Component {
    constructor() {
        super();
    }
    componentWillMount() {
    }
    componentWillUpdate(nextProps) {
    }

    componentDidMount() {
    }

    render() {
        const { formatMessage } = this.props.intl;
        const { handleSubmit, pristine, error, reset, submitting } = this.props

        return (
            <form onSubmit={handleSubmit}>
                {/*
                <Field name="favoriteColor" label="Country" component={renderSelectField}>
                    <option value="">Please Select Country </option>
                    <option value="ff0000">Red</option>
                    <option value="00ff00">Green</option>
                    <option value="0000ff">Blue</option>
                </Field>
                */}

                <Field className="form-control textBox" name="userName" component={renderField} type="text" label="User Name" />
                <Field className="form-control textBox" name="password" component={renderField} type="password" label="Password" />
                <div className="form-group form-inline">
                    <label htmlFor="dummy"></label>
                    <button type="submit" disabled={pristine || submitting} className="btn btn-success"><i className="fa fa-check-circle"></i> Login</button>
                </div>
                {error && <div className="alert alert-danger text-center">{error}</div>}
            </form >
        );
    }
    componentWillUnmount() {
    }
}

LoginForm.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
    const temp = state.getIn(['home', 'temp']);
    return {
        temp
    }
}

export default compose(injectIntlDecorator(),
    reduxForm({ form: 'login', validate }),
    connect(mapStateToProps, actions, null, { pure: false })
)(LoginForm);
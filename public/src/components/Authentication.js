import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
/**
 * A higher order component that let the wrapped component load only if the user is authenticated
 */
export default function (ComposedComponent) {
    class Authentication extends Component {
        static contextTypes = {
            router: PropTypes.object
        }
        componentWillMount() {
            if (!this.props.splashScreenVisited) {
                browserHistory.push('/')
            }
        }

        componentWillUpdate(nextProps) {
            if (!this.props.splashScreenVisited) {
                browserHistory.push('/')
            }
        }

        render() {
            return this.props.splashScreenVisited ? <ComposedComponent {...this.props} /> : null
        }
    }

    function mapStateToProps(state) {
        let splashScreenVisited = state.getIn(['home', 'splashScreenVisited']);
        return { splashScreenVisited: splashScreenVisited };
    }

    return connect(mapStateToProps)(Authentication);
}


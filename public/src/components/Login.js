import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import { browserHistory } from 'react-router';
import * as actions from '../actions';
import LoginForm from './LoginForm'
import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';

const labels = defineMessages({
    test: {
        id: 'test',
        defaultMessage: "Test"
    }
});

class Login extends Component {
    constructor() {
        super();
    }
    componentWillMount() {
        if (this.props.isLoggedIn)
            browserHistory.push("/home");
    }
    componentWillUpdate(nextProps) {
    }

    componentDidMount() {

    }

    submit = (values) => {
        const data = values.toJS();
        return this.props.checkLogin(data)
    }

    render() {
        const { formatMessage } = this.props.intl;
        return (
            <div className="loginPanel">
                <div className="centered">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4>CND Chat bot Admin - Login</h4>
                        </div>
                        <div className="panel-body">
                            <LoginForm onSubmit={this.submit} />

                        </div>
                    </div>
                </div >
            </div>
        );
    }
    componentWillUnmount() {
    }
}

Login.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
    const isLoggedIn = state.getIn(['home', 'isLoggedIn']);
    return {
        isLoggedIn
    }
}

export default compose(injectIntlDecorator(),
    connect(mapStateToProps, actions, null, { pure: false })
)(Login);

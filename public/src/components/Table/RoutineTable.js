import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import { browserHistory } from 'react-router';
//import RegionsEditor from './RegionsEditor';

const priceFormatter = (image, { title }) => {
    return <div>
        <img style={{ height: "100px", display: "block", margin: "0 auto" }} src={image} />
        {/* <div style={{ left: "0px", right: "0px", bottom: "0px", height: "30px", color: "rgb(255, 255, 255)", background: "rgba(0, 0, 0, 0.4)", alignItems: "center", textAlign: "center" }}>
            <div style={{ fontSize: "16px", textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }} >
                {title}
            </div>
        </div> */}
    </div>
}
// const createRegionsEditor = (onUpdate, props) => (<RegionsEditor onUpdate={onUpdate} {...props} />);
const cellEditProp = {
    mode: 'click',
    blurToSave: true
};
export default class RoutineTable extends React.Component {

    componentWillMount() {
        // this.props.updateModelPopup()
    }

    render() {
        const jobTypes = ['A', 'B', 'C', 'D'];
        return (
            <div>
                <BootstrapTable cellEdit={cellEditProp} data={this.props.data}>
                    <TableHeaderColumn dataField='id' isKey={true}>Id</TableHeaderColumn>
                    <TableHeaderColumn dataField='title'>Title</TableHeaderColumn>
                    <TableHeaderColumn dataFormat={priceFormatter} dataField='image'>Image</TableHeaderColumn>
                    <TableHeaderColumn dataField='link'>link</TableHeaderColumn>
                    {/*insertRow={true} <TableHeaderColumn customEditor={{ getElement: createRegionsEditor }}
                                dataField='sortOrder'>sortOrder</TableHeaderColumn> */}
                    <TableHeaderColumn editable={{ type: 'select', options: { values: jobTypes } }}
                        dataField='sortOrder'>SortOrder</TableHeaderColumn>

                </BootstrapTable>
            </div>
        );

    }
}

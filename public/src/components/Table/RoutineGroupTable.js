import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import { browserHistory } from 'react-router';
import * as actions from '../../actions';
import { Link } from 'react-router';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';

const labels = defineMessages({
    test: {
        id: 'test',
        defaultMessage: "Test"
    }
});
const productFormatter = ({ image, title, link }, { productId }) => {
    return <div>
        <img style={{ height: "100px", display: "block", margin: "0 auto" }} src={image} />
        <div style={{ left: "0px", right: "0px", bottom: "0px", height: "30px", color: "rgb(255, 255, 255)", background: "rgba(0, 0, 0, 0.4)", alignItems: "center", textAlign: "center" }}>
            <Link to={{ pathname: '/product', search: `?${productId}` }} >
                <div style={{ fontSize: "16px", textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }} >
                    {title}
                </div>
            </Link>
        </div>
    </div>
}
const viewRoutineItem = ({ image, title, link }, { id, routines }) => {
    return <Link to={{ pathname: `/home/routine/${id}` }}>
        {routines && routines.length > 0 ? `View/Modify` : `Add`}
    </Link>

}

class RoutineGroupTable extends Component {
    constructor() {
        super();

    }
    componentWillMount() {
        this.props.getData('routineGroup');
    }
    componentWillUpdate(nextProps) {
    }

    componentDidMount() {

    }
    render() {
        const { formatMessage } = this.props.intl;
        return (
            <div>
                <BootstrapTable data={this.props.data} striped hover condensed>
                    <TableHeaderColumn headerAlign='center' width='50' isKey={true} dataField='id'>S.No</TableHeaderColumn>
                    <TableHeaderColumn headerAlign='center' width='150' dataField='name'>Name</TableHeaderColumn>
                    <TableHeaderColumn headerAlign='center' width='150' dataField='skinType'>Skin Type</TableHeaderColumn>
                    <TableHeaderColumn headerAlign='center' width='150' dataField='skinIssue'>Skin Issue</TableHeaderColumn>
                    <TableHeaderColumn headerAlign='center' width='250' dataFormat={productFormatter} dataField='product'>Product</TableHeaderColumn>
                    <TableHeaderColumn headerAlign='center' width='120' dataFormat={viewRoutineItem} dataField='product'>View <br />Routine Item</TableHeaderColumn>
                </BootstrapTable>
            </div>);
    }
    componentWillUnmount() {
    }
}

RoutineGroupTable.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
    const routineGroup = state.getIn(['home', 'routineGroup']);
    return {
        routineGroup: (routineGroup && routineGroup.toJS()) || []
    }
}

export default compose(injectIntlDecorator(),
    connect(mapStateToProps, actions, null, { pure: false })
)(RoutineGroupTable);





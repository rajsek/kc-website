import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import curryRight from 'lodash.curryright';
import { browserHistory } from 'react-router';
import { Field, reduxForm } from 'redux-form/immutable';
import { Link } from 'react-router';
import { FontIcon, Dialog, FlatButton, TextField, RaisedButton } from 'material-ui';

import AddRoutine from './Form/AddRoutine';
import RoutineTable from './Table/RoutineTable';
import * as actions from '../actions';

import {
    defineMessages,
    FormattedMessage,
    FormattedDate,
    injectIntl,
    intlShape
} from 'react-intl';

const labels = defineMessages({
    test: {
        id: 'test',
        defaultMessage: "Test"
    }
});
const style = {
    margin: 12,
};

class Routine extends Component {
    constructor() {
        super();
        this.state = {
            value: 'Property Value',
        };
    }
    componentWillMount() {
        const { params } = this.props
        const routineGroupId = params && params.routineGroupId || 1;
        this.props.getData('routineGroup');
        this.props.updateModelPopup(true, routineGroupId)
    }
    componentWillUpdate(nextProps) {
    }

    componentDidMount() {
    }
    submit = (values) => {
        const data = values.toJS();
        return this.props.saveRoutine(data);
    }
    render() {
        const { formatMessage } = this.props.intl;
        const { handleSubmit, pristine, error, reset, submitting, params } = this.props
        const { routineGroup, addRoutineItem } = this.props;
        const routineGroupId = params && params.routineGroupId || 1;
        const selectedRoutineGroup = routineGroup.filter(routine => {
            return routine.id == routineGroupId
        })

        const actionsButton = [
            <FlatButton
                label="Save"
                primary={true}
                keyboardFocused={true}
                onTouchTap={() => this.props.submitAddFrom('addRoutine')}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={() => this.props.setModelPopupVisibility()}
            />

        ];
        return (<div>
            {selectedRoutineGroup.length > 0 ?
                <div style={{ margin: "5px" }}>
                    <h3 style={{ textAlign: "center" }}>
                        <span>{`Please add routine item for `}</span>
                        <span style={{ color: '#00bcd4' }}>
                            {`${selectedRoutineGroup[0].name}(${selectedRoutineGroup[0].skinType}-${selectedRoutineGroup[0].skinIssue})`}
                        </span>
                    </h3>
                    <RaisedButton
                        onClick={() => {
                            this.props.setModelPopupVisibility('addRoutineItem');
                            {/* this.props.updateModelPopup(true, routineGroupId)
                            browserHistory.push(`home/addRoutine/${routineGroupId}`) */}
                            //console.log("Move to Add routine Component" + this.props.data[0].routineGroupId)
                        }}
                        label="Add Routine"
                        icon={<FontIcon className="material-icons">add_circle_outline</FontIcon>}
                        primary={true}
                        style={{ marginBottom: 12 }}
                    />
                    <Dialog
                        title="Add New Routine Group"
                        actions={actionsButton}
                        modal={false}
                        open={addRoutineItem}
                        onRequestClose={() => this.props.setModelPopupVisibility()}
                        autoScrollBodyContent={true}>
                        <AddRoutine onSubmit={this.submit} />
                    </Dialog>
                    <RoutineTable data={selectedRoutineGroup[0].routines} />
                </div>
                :
                <div style={{ margin: "5px" }}>
                    <h3 style={{ textAlign: "center" }}>
                        <span>{`Invalid Selection move to `}
                            <Link to='/home'>routine group</Link>
                        </span>
                    </h3>
                </div>
            }
        </div>
        );
    }
    componentWillUnmount() {
    }
}

Routine.propTypes = {
    intl: intlShape.isRequired
};
const injectIntlDecorator = curryRight(injectIntl);

function mapStateToProps(state, ownProps) {
    const routineGroup = state.getIn(['home', 'routineGroup']);
    const addRoutineItem = state.getIn(['home', 'showAddModelPopup', 'addRoutineItem']);

    return {
        routineGroup: routineGroup && routineGroup.toJS() || [],
        addRoutineItem: addRoutineItem || false

    }
}

export default compose(injectIntlDecorator(),
    connect(mapStateToProps, actions, null, { pure: false })
)(Routine);
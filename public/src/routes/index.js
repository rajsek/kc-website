import React from "react";
import { Route, IndexRoute, Switch } from "react-router";
import App from "../containers/App";
import Home from "../components/Home";
import Login from "../components/Login";
import RoutineGroup from "../components/RoutineGroup";
import Routine from "../components/Routine";
import Authentication from "../components/Authentication";

const NoMatch = ({ location }) =>
  <div style={{ textAlign: "center", margin: "25%" }}>
    <h3>404 page not found</h3>
    <p>We are sorry but the page you are looking for does not exist.</p>
  </div>;

export default (
  <Route path="/" component={App}>
    {/* <IndexRoute component={Login} /> */}
    {/* <Route path="/" component={Login} /> */}
    <Route path="home" component={Home}>
      <IndexRoute component={RoutineGroup} />
      <Route path="routineGroup" component={RoutineGroup} />
      <Route path="routine/:routineGroupId" component={Routine} />
    </Route>
    <Route path="*" component={NoMatch} />
  </Route>
);

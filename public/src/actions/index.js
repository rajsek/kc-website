import { SAMPLE } from "./actionTypes";
import cookies from "cookie-jeep";
import axios from "axios";
import { browserHistory } from "react-router";
import { SubmissionError } from "redux-form/immutable";
import { submit } from "redux-form";
import Alert from "react-s-alert";

/* Previous Code*/

export function sampleAction(locale) {
  return dispatch => {
    dispatch({ type: SAMPLE });
  };
}

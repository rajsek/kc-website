import { SAMPLE } from "../actions/actionTypes";

import { fromJS, Map } from "immutable";
import cookies from "cookie-jeep";

import conf from "../conf";

// var default_locale = defaultLocale != "{{defaultLocale}}" ? defaultLocale : 'en_US';
// var is_show_lang_switch = isShowLangSwitch != "{{isShowLangSwitch}}" ? isShowLangSwitch : 'true';

// const locale = conf.i18n.langs.indexOf(cookies.read('locale')))
//     ? cookies.read('locale')
//     : default_locale;

function getNewState(state, newState) {
  return state.merge(newState);
}
function removeActionType(obj) {
  let copy = Object.assign({}, obj);
  delete copy["type"];
  return copy;
}
export default function(
  state = fromJS({
    sample: null
  }),
  action
) {
  switch (action.type) {
    case SAMPLE:
      return getNewState(state, { sample: "SAMPLE" });
    default:
      console.log(`action.type : ${action.type}`);
      return state;
  }
}

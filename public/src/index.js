import "babel-polyfill"; // This polyfill is to support some ES6 tags in browsers like IE11 and below
import React from "react";
import MobileDetect from "mobile-detect";
import arrayFrom from "array-from";
import ReactDOM from "react-dom";
import { Router, browserHistory } from "react-router";
import reduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./rootReducer";
import routes from "./routes";
import { syncHistoryWithStore, routerReducer } from "react-router-redux";
import Immutable from "immutable";
import ConnectedIntlProvider from "./libraries/ConnectedIntlProvider";
import conf from "./conf";
import "./libraries/tap_events";

import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/jelly.css";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// require("bootstrap/dist/css/bootstrap.css");
// require("bootstrap");
// js
// import "jquery";
// import "bootstrap";
require("../assets/scss/style.scss");

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941

//require('../css/bootstrap.min.css');

//Array.from polyfill
Array.from = Array.from || arrayFrom;

//Apply your middleware before creating the store
const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

//Make the store support immutable types
const initialState = Immutable.Map();

// Create an enhanced history that syncs navigation events with the store.
const store = createStoreWithMiddleware(
  reducers,
  initialState,
  // Enable redux dev tools
  window.devToolsExtension && window.devToolsExtension()
);

// Create an enhanced history that syncs navigation events with the store.
const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState(state) {
    return state.get("routing").toJS();
  }
});

const target = document.querySelector("#main");

function logPageView() {}

class Root extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <Provider store={store}>
          <ConnectedIntlProvider>
            <Router history={history} onUpdate={logPageView}>
              {routes}
            </Router>
          </ConnectedIntlProvider>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

const md = new MobileDetect(navigator.userAgent);
const is_mobile = md.mobile() !== null;
//Explicitly check  and activate HMR
if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(<Root />, target);

const models = require('./models');
require('dotenv').config({ path: __dirname + '/.env', silent: true })
var Sequelize = require('sequelize');

//set all model associations
models.routineGroup.hasMany(models.routine, {
    foreignKey: 'routineGroupId'
});

models.routineGroup.belongsTo(models.product, {
    foreignKey: 'productId'
});

// console.log(models.product.findAll({
//     include: [{
//         model: models.routineGroup,
//         where: { id: Sequelize.col('routineGroups.productId') }
//     }]
// }));
console.log(models.routineGroup.findAll({
    attributes: { exclude: ['locale'] },
    include: [{
        attributes: { exclude: ['id', 'locale', 'sortOrder'] },
        model: models.product, where: { productId: Sequelize.col('product.id') }

    }]
}));
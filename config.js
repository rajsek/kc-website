"use strict";
let config = {};

config = {
  url: "test"
};

config.i18n = {
  lang: ["en_US", "id_ID"],
  default_lang: "en_US"
};
config.logLevel = process.env.LOG_LEVEL || "verbose";
module.exports = config;

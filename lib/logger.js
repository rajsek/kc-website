"use strict";

const winston = require("winston");
const config = require("../config");

module.exports = new winston.Logger({
  transports: [
    new winston.transports.Console({
      timestamp: true,
      level: config.logLevel || "info"
    }),
    new winston.transports.File({
      name: "info-file",
      filename: "filelog-info.log",
      level: "info"
    }),
    new winston.transports.File({
      name: "error-file",
      filename: "filelog-error.log",
      level: "error"
    })
  ]
});

'use strict';
module.exports = function (sequelize, DataTypes) {
  var user = sequelize.define('user', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.BIGINT(20)
    },
    pageId: DataTypes.STRING,
    locale: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    gender: DataTypes.STRING,
    ageGroup: DataTypes.STRING,
    skinType: DataTypes.STRING,
    skinIssue: DataTypes.STRING,
    subscription: DataTypes.STRING,
    skinTipData: DataTypes.STRING,
    currentProgress: {
      type: DataTypes.ENUM,
      values: [
        'INITIATED',
        'SELECTED_AGE',
        'SELECTED_SKIN_TYPE',
        'SELECTED_SKIN_ISSUE',
        'SKIN_EXPERT_SUBSCRIBED',
        'COMPLETED_SKIN_EXPERT_FLOW',
        'INITIATED_SELFIE_SOUNDTRACK_FLOW', // Clicked yes for selfie upload
        'COMPLETED_SELFIE_SOUNDTRACK_FLOW',
        'INITIALIZED_SKIN_TIP_FLOW',
        'INITIALIZED_PRODUCTS_AND_PROMO_FLOW'
      ]
    },
  }, {
    timestamps: false,
    classMethods: {
      associate: function (models) {}
    }
  });
  return user;
};